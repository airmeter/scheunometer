#ifndef _DATA_H
#define _DATA_H

#include <stdint.h>
#include <time.h>

struct data {
  struct tm datetime;
  float temperature;
  uint8_t humidity;
  bool shower;
  bool window_open;
};

#endif