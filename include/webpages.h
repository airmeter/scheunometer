static const char mainpage[] PROGMEM = R"=====(
<!DOCTYPE html><html>
<head>
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <link rel="icon" href="data:,">
 <style>
  fieldset {
    max-width: 5%%;
  }
 </style>
 <script>
  // Data-Updates
  setInterval(function() {
    getData();
  }, 10000);

  function getData() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        document.getElementById('temp').innerHTML = this.responseText.split(';')[0];
        document.getElementById('humidity').innerHTML = this.responseText.split(';')[1];
      }
    };
    xhttp.open('GET', 'getdata', true);
    xhttp.send();
  }

  function send_request(value) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) { }
    };
    xhttp.open('GET', value, true);
    xhttp.send();
  }
 </script>
</head>
<body>
 <h1>Klomometer</h1>
 <fieldset class="fsmain">
  <legend>Temperatur</legend>
  <span id="temp">%4.1f</span> &deg;C
 </fieldset>
 <fieldset class="fsmain">
  <legend>Luftfeuchtigkeit</legend>
  <span id="humidity">%d</span> &#37;
 </fieldset>
 <fieldset class="fsmain">
  <legend>Einstellungen</legend>
  <button onclick=send_request("reset_shower")>Duschen zur&uuml;cksetzen</button>
  <button onclick=send_request("reset_window")>Fenster schlie&szlig;en</button>
 </fieldset>
</body></html>
)=====";
