#include <UniversalTelegramBot.h>
#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>
#include "telegram.h"
#include "personal_data.h"

WiFiClientSecure client;
UniversalTelegramBot bot(botToken, client);

/*float* tele_temperature;
uint8_t* tele_humidity;
bool* tele_shower;
bool* tele_window_open;*/
struct data* tele_data;
struct tm* tele_time;

TELEGRAM::TELEGRAM(struct data* _data, struct tm* _time) {
  ;// /*float* _temp, uint8_t* _humid, bool* _shower, bool* _window_open*/, struct tm* _time) {
  /*tele_temperature = _temp;
  tele_humidity = _humid;
  tele_shower = _shower;
  tele_window_open = _window_open;*/
  tele_data = _data;
  tele_time = _time;
  client.setInsecure();
}

void TELEGRAM::SendMessage(uint8_t id, char txt[]) {
  SendMessage(telegram_ids[id], txt);
}


void TELEGRAM::SendMessage(String id, char txt[])
{
  Serial.printf("Length: '%d'\n", sizeof(telegram_ids) / sizeof(telegram_ids[0]));
  for (uint8_t i = 0; i < sizeof(telegram_ids) / sizeof(telegram_ids[0]); i++)
    if (telegram_ids[i] == id) {
      bot.sendMessage(telegram_ids[i], txt, "");
      return;
    }
  Serial.printf("Telegram-ID unknown: '%'\n", id.c_str());
}

void TELEGRAM::handleNewMessages(int numNewMessages)
{
  for (int i = 0; i < numNewMessages; i++)
  {
    String chat_id = bot.messages[i].chat_id;
    String from_id = bot.messages[i].from_id;
    String text = bot.messages[i].text;
    String from_name = bot.messages[i].from_name;
    Serial.printf("Telegram-Message:\nchat_id: %s\nfrom_id: %s\nFrom: %s\ntext: %s\n\n", chat_id.c_str(), from_id.c_str(), from_name.c_str(), text.c_str());

    if (from_name == "")
      from_name = "Guest";

    if (bot.messages[i].text == "/reset_shower") {
      tele_data->shower = 0;
      Serial.printf("Dusche zurueckgesetzt\n");
      bot.sendMessage(chat_id, "Dusche zurueckgesetzt", "");
    }
    else if (bot.messages[i].text == "/reset_window") {
      tele_data->window_open = 0;
      Serial.printf("Fenster zurueckgesetzt\n");
      bot.sendMessage(chat_id, "Fenster zurueckgesetzt", "");
    }
    else if (bot.messages[i].text == "/status") {
      char txt[300];
      sprintf(txt, "Wir haben den %02u.%02u.%u und es ist %02u:%02u:%02u. Es sind %2.1f °C und wir haben eine Luftfeuchtigkeit von %u %%. Es wird %sund %s.", tele_time->tm_mday, tele_time->tm_mon + 1, tele_time->tm_year + 1900, tele_time->tm_hour, tele_time->tm_min, tele_time->tm_sec, tele_data->temperature, tele_data->humidity, tele_data->shower ? "aktuell geduscht " : "aktuell nicht geduscht ", tele_data->window_open ? "das Fenster ist offen" : "das Fenster ist zu");
      bot.sendMessage(chat_id, txt, "");
    }
    else if (text == "/options")
    {
      //String keyboardJson = "[[{ \"text\" : \"Go to Google\", \"url\" : \"https://www.google.com\" }],[{ \"text\" : \"Send\", \"callback_data\" : \"This was sent by inline\" }]]";
      String keyboardJson = "[";
      keyboardJson += "[{ \"text\" : \"Duschen beenden\", \"callback_data\" : \"/reset_shower\" }],";
      keyboardJson += "[{ \"text\" : \"Fenster schliessen\", \"callback_data\" : \"/reset_window\" }],";
      keyboardJson += "[{ \"text\" : \"Aktuellen Status\", \"callback_data\" : \"/status\" }]";
      keyboardJson += "]";
      bot.sendMessageWithInlineKeyboard(chat_id, "Choose from one of the following options", "", keyboardJson);
    }
    else if (text == "/start")
    {
      String welcome = "Wilkommen beim Klomometer! Ich liefere aktuelle Daten aus deinem Badezimmer ;)\n";
      welcome += "\n/window_closed : Setzt die Fenster-Offen-Variable zurück auf geschlossen\n";
      welcome += "/shower_ended : Setzt die Dusche-Variable zurück auf: 'Nicht am Duschen'\n";
      bot.sendMessage(chat_id, welcome, "Markdown");
    }
  }
}

void TELEGRAM::CheckMessages() {
  int numNewMessages = bot.getUpdates(bot.last_message_received + 1);

  while (numNewMessages)
  {
    Serial.println("got telegram response");
    handleNewMessages(numNewMessages);
    numNewMessages = bot.getUpdates(bot.last_message_received + 1);
  }
}